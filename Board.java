public class Board {
private Die d1;
private Die d2;
//private boolean[] closedTiles;
final int LOSER = 3;
private int[][] threeStrikes;

public Board() {

	this.d1 = new Die();
	this.d2 = new Die();
	//this.closedTiles = new boolean[12];
	this.threeStrikes = new int[6][6];
}


public boolean playATurn(){
	int count = 0;
	boolean yesNo = false;
	
	while (count < LOSER) {
		int die_One = this.d1.roll();
		int die_Two = this.d2.roll();
		int i = die_One -1;
		int j = die_Two -1;
		int sum = die_One + die_Two;
		
			System.out.println("\n" + die_One +" + " + die_Two + " = " + sum);
			
			if(this.threeStrikes[i][j] != 0){
			count = count +1;
			}
				
			this.threeStrikes[i][j] = die_Two;	
			System.out.println("[" + die_One + "][" + die_Two + "]");
		}
		
		if (count == LOSER) {
		yesNo = true;	
			System.out.println("Game Over");	
	}	
	return yesNo;
}

	/*
	for (int i = 0; i < this.closedTiles.length; i++){
		if (i == sum -1 && this.closedTiles[i] == true){
		System.out.println("Tile already shut.");
		System.out.println(toString());
		yesNo = true;
		}
		
		else if (i == sum -1){
			System.out.println("Closing tile " + sum + "...");
			this.closedTiles[sum -1] = true;
			System.out.println(toString());
			yesNo = false;
		}
	}*/ 

public String toString(){
	String s = "";
	
	
	for (int i = 0; i < this.threeStrikes.length; i++){
		for (int j = 0; i < this.threeStrikes[i].length; j++){
			s += " " + this.threeStrikes[i][j];
		}
	}
	/*
	
	for (int i = 0; i < this.closedTiles.length; i++){
		if (this.closedTiles[i] == true){
			s = s + "X ";
		}
		else {
			s = s + (i+1) + " ";
		}
	}
	*/
	return s;
}	
}