import java.util.Random;
public class Die {
private int pips;
private Random rnd;

	public Die() {
		this.pips = 1;
		this.rnd = new Random();
	}

	public int getPips(){
		return roll();
	}

	public int roll(){
		this.pips = 1 + this.rnd.nextInt(6);
		return this.pips;
	}
	
	public String toString(){
		String s = "";
		/*
		for (int i = 0; i < 15; i++){
			s = s +getPips() +"\n";
		}
		return s; 
		*/
		return s + getPips();
	}
}