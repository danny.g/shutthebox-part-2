public class ShutTheBox{
	public static void main (String[]args) {

		System.out.println("Welcome to shut the box, where the only way to avoid death is to win");
		Board b = new Board();
		boolean gameOver = false;
		
		while ((gameOver != true)) {
			System.out.println("\nPlayer 1's turn");
			//b.playATurn();
			if (b.playATurn() == true){
				System.out.println("\nPlayer 2 wins");
				gameOver = true;
				}
				
				else {
				System.out.println("\nPlayer 2's turn");
				//b.playATurn();
				if (b.playATurn() == true){
					System.out.println("\nPlayer 1 wins");
					gameOver = true;
				}
			}
		}				
	}
}